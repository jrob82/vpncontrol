# Changelog

## 2021/08/21
* fixed: bash path in airvpn.service is now correct

## 2020/11/26
* added: option to connect to random server of specific country
* changed: after unsuccessfull connection attempt the screen won't return to the main screen, but to the screen of the previous selection (if applicable, i. e. for connections to specific servers or countries)

## 2020/10/28
* fixed: changed systemd unit's service type from forking to simple since forking didn't seem to work universally (fixes #2)
* changed: when refreshing the user info in the VPNControl.sh script, the script now checks if Hummingbird is running before attempting to fetch the data to avoid an inevitable timeout if not connected with the default network lock enabled

## 2020/08/26
* added: option to not use AirVPN's network lock
* added: config setting for number of connection attempts when trying to connect to recommended or random server
* fixed: removed confirmation prompt for deleting log file which sometimes accidentally appears

## 2020/06/26
* fixed: bug in boot script that made it exit when custom Hummingbird options were set
* changed: systemd unit now looks for bash at /bin/bash instead of /usr/bin/bash

## 2020/06/16
* added: menu to set custom Hummingbird options (temporary until script exits, button to make options permanent) and config file entry for permanent custom Hummingbird options
* added: permanent default network lock (activating the default network lock in the interface only has effect until reboot; new config file option makes it permanent by writing lock rules at boot)
* fixed: bug which led to deleting older log files while cycling through failing connection attempts
* fixed: connection after selecting a country
* changed: removed numbers in country and server list to be able to navigate by typing first letter of country/server
